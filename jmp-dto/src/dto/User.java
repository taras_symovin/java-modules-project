package dto;

import java.time.LocalDate;
import java.time.Period;

public record User(String name, String surname, LocalDate birthday) {

    public int getAge() {
        return Period.between(birthday, LocalDate.now()).getYears();
    }

    @Override
    public String toString() {
        return "User{" +
            "name='" + name + '\'' +
            ", surname='" + surname + '\'' +
            ", birthday=" + birthday +
            '}';
    }
}