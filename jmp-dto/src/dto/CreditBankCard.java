package dto;

public class CreditBankCard extends BankCard {
    private final double creditLimit;

    public CreditBankCard(String number, User user, double creditLimit) {
        super(number, user);
        this.creditLimit = creditLimit;
    }

    public double getCreditLimit() {
        return creditLimit;
    }
}