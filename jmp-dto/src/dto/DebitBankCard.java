package dto;

public class DebitBankCard extends BankCard {
    private final double balance;

    public DebitBankCard(String number, User user, double balance) {
        super(number, user);
        this.balance = balance;
    }

    public double getBalance() {
        return balance;
    }
}