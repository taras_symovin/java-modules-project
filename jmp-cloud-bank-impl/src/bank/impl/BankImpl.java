package bank.impl;

import bank.api.Bank;
import dto.BankCard;
import dto.BankCardType;
import dto.CreditBankCard;
import dto.DebitBankCard;
import dto.User;

import java.util.Random;

public class BankImpl implements Bank {
    @Override
    public BankCard createBankCard(User user, BankCardType type) {
        String number = generateCardNumber();
        if (type == BankCardType.CREDIT) {
            return new CreditBankCard(number, user, 1000.0);
        } else {
            return new DebitBankCard(number, user, 0.0);
        }
    }

    private String generateCardNumber() {
        Random random = new Random();
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < 16; i++) {
            stringBuilder.append(random.nextInt(10));
        }
        return stringBuilder.toString();
    }
}