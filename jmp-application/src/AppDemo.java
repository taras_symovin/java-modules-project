import bank.api.Bank;
import bank.impl.BankImpl;
import dto.BankCardType;
import dto.Subscription;
import dto.User;
import service.api.Service;
import service.impl.ServiceImpl;

import java.time.LocalDate;
import java.util.List;

public class AppDemo {
  public static void demo() {
    Service service = new ServiceImpl();
    Bank bank = new BankImpl();

    User user = new User(
        "Taras",
        "Symovin",
        LocalDate.of(2001, 1, 30)
    );

    User user2 = new User(
        "Iryna",
        "Tsypka",
        LocalDate.of(2004, 5, 18)
    );

    var bankCard = bank.createBankCard(user, BankCardType.DEBIT);
    var bankCard2 = bank.createBankCard(user2, BankCardType.CREDIT);

    service.subscribe(bankCard);
    service.subscribe(bankCard2);

    System.out.println(Service.isPayableUser(user));

    System.out.println(service.getSubscriptionByBankCardNumber(bankCard.getNumber()));
//    System.out.println(service.getSubscriptionByBankCardNumber("1111"));

    List<Subscription> subscriptions = service.getAllSubscriptionsByCondition(
        subscription -> subscription.startDate()
            .isAfter(LocalDate.of(2023, 10, 22)));

    System.out.println("Subscriptions after 2023-10-22: " + subscriptions);

    System.out.println(bankCard);

    System.out.println(service.getAllUsers());

    System.out.println(service.getAverageUsersAge());
  }
}