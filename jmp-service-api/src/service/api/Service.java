package service.api;

import dto.BankCard;
import dto.Subscription;
import dto.User;

import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.function.Predicate;

public interface Service {
    void subscribe(BankCard bankCard);
    Optional<Subscription> getSubscriptionByBankCardNumber(String number);
    List<User> getAllUsers();

    List<Subscription> getAllSubscriptionsByCondition(Predicate<Subscription> condition);

    default OptionalDouble getAverageUsersAge(){
        return getAllUsers().stream().mapToInt(User::getAge).average();
    }

    static boolean isPayableUser(User user){
        return user.getAge() >= 18;
    }
}