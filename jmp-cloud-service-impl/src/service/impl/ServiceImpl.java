package service.impl;

import dto.BankCard;
import dto.Subscription;
import dto.User;
import exceptions.SubscriptionNotFoundException;
import service.api.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;

public class ServiceImpl implements Service {
    private final Map<String, Subscription> subscriptions = new HashMap<>();
    private final List<User> users = new ArrayList<>();

    @Override
    public void subscribe(BankCard bankCard) {
        Subscription subscription = new Subscription(bankCard.getNumber(), LocalDate.now());
        subscriptions.put(bankCard.getNumber(), subscription);
        users.add(bankCard.getUser());
    }

    @Override
    public Optional<Subscription> getSubscriptionByBankCardNumber(String number)  {
        return Optional.ofNullable(Optional.ofNullable(subscriptions.get(number))
            .orElseThrow(() -> new SubscriptionNotFoundException("Subscription Not Found Exception")));
    }

    @Override
    public List<User> getAllUsers() {
        return users;
    }

    @Override
    public List<Subscription> getAllSubscriptionsByCondition(Predicate<Subscription> condition) {
        List<Subscription> subscriptions = new ArrayList<>(this.subscriptions.values());
        List<Subscription> filteredSubscriptions = new ArrayList<>();
        for (Subscription subscription : subscriptions) {
            if (condition.test(subscription)) {
                filteredSubscriptions.add(subscription);
            }
        }
        return filteredSubscriptions;
    }
}